n = int(input())
matrix = [[0 if (i + j - 1) % 4 else 1 for j in range(n)] for i in range(n)]

for line in matrix:
    print(*line)
