count_of_stations = int(input())
count_of_people = 3
peregons = [0] * (count_of_stations - 1)

for i in range(count_of_people):
    passenger = input().split()[2:]
    first_station = int(passenger[0])
    last_station = int(passenger[1])
    for j in range(first_station - 1, last_station - 1):
        peregons[j] += 1

max_pass = 0
for peregon in peregons:
    if peregon > max_pass:
       max_pass = peregon

for i in range(count_of_stations-1):
    if peregons[i] == max_pass:
        print((i + 1), '-', (i + 2))
